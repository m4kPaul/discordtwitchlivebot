# (shitty) Discord Twitch Live Bot

## Initial setup

1. Install current [Node.js](https://nodejs.org/en/download/current/) for your OS.
2. Open bot folder in the console.
     
     e.g.:
    - (shift + right mouse button) -> 'Open PowerShell window here'
    - start CMD -> `cd path/to/bot`
3. Run `npm install`
4. Update `config.json`
    - Twitch App Client ID + Secret: https://dev.twitch.tv/console
    - Discord bot token: https://discord.com/developers/applications

## Run the bot
1. Open bot folder in the console.
2. Run `node bot.js`