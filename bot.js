const { ClientCredentialsAuthProvider } = require('twitch-auth');
const { ApiClient } = require('twitch');
const fs = require('fs').promises;
const fss = require('fs');

const Discord = require('discord.js');
const discordClient = new Discord.Client();

const config = JSON.parse(fss.readFileSync('./config.json', 'UTF-8'));
const prefix = config.DiscordData.prefix;
const streamers = JSON.parse(fss.readFileSync('./streamers.json', 'UTF-8'));

const twitchAuthProvider = new ClientCredentialsAuthProvider(config.TwitchData.clientId, config.TwitchData.clientSecret);
const twitchClient = new ApiClient({ authProvider: twitchAuthProvider });


discordClient.on('ready', async () => {
  for (let i = 0; i < streamers.length; i++) streamers[i].isStreaming = false;
  await fs.writeFile('streamers.json', JSON.stringify(streamers, ' ', 2))
         .catch(err => console.log(err));
  console.log('Ready');
  setInterval(() => {
    checkStreams();
  }, 15 * 1000);
});

discordClient.on('message', async (message) => {
  if (!message.content.startsWith(prefix) ||
      !message.member.hasPermission('ADMINISTRATOR') ||
      message.author.bot) return;

	const args = message.content.slice(prefix.length).trim().split(' ');
	const command = args.shift().toLowerCase();

  if (command === 'ping') {
    return message.reply(`Pong!`);
  }

  if (command === 'help') {
    return message.channel.send({ "embed": {
      "title": "Available Commands",
      "description": `${prefix}help - sends this message.\n${prefix}ping - Pong!\n${prefix}addstreamer <@User> <Twitch Link>\n${prefix}setkeyphrase <Stream Title Keyword(s)>\n${prefix}setrole <@&Role>\n${prefix}setchannel <#Channel>`,
      "color": 5025616
    }});
  }

  if (command === 'addstreamer') {
    if (args.length !== 2 || !message.mentions.users.size) return message.channel.send(`Usage: ${prefix}addstreamer <@User> <Twitch Link>`);
    const discordUser = message.mentions.users.first();
    const twitchUser = await twitchClient.helix.users.getUserByName(args[1].split('/').pop());
    if (!twitchUser) return message.channel.send(`Failed to get Twitch user`);
    if (streamers.findIndex(s => s.twitchUserName.toLowerCase() === twitchUser.name.toLowerCase()) !== -1) console.log(`Adding duplicate streamer - ${twitchUser.name}`);
    const newStreamer = { twitchUserName: twitchUser.name, isStreaming: false, discordUserId: discordUser.id };
    streamers.push(newStreamer);
    await fs.writeFile('streamers.json', JSON.stringify(streamers, ' ', 2))
           .catch(err => console.log(err))
           .then(message.reply(`Added new streamer: <@${newStreamer.discordUserId}> - ${newStreamer.twitchUserName}`));
  }
  
  if (command === 'setkeyphrase') {
    if (args.length < 1) return message.channel.send(`Usage: ${prefix}setkeyphrase <Stream Title Keyword(s)>`);
    let name = '';
    for (let i = 0; i < args.length; i++) {
      name += args[i];
      if (i !== args.length - 1) name += " ";
    }
    config.TwitchData.keyphrase = name;
    await fs.writeFile('config.json', JSON.stringify(config, ' ', 2))
           .catch(err => console.log(err))
           .then(message.reply(`Updated stream keyphrase to: ${name}`));
  }

  if (command === 'setrole') {
    if (args.length !== 1 || !message.mentions.roles.size) return message.channel.send(`Usage: ${prefix}setrole <Role>`);
    const role = message.mentions.roles.first();
    config.DiscordData.roleId = role.id;
    await fs.writeFile('config.json', JSON.stringify(config, ' ', 2))
           .catch(err => console.log(err))
           .then(message.reply(`Updated streamer role to: ${role.name}`));
  }

  if (command === 'setchannel') {
    if (args.length !== 1 || !message.mentions.channels.size) return message.channel.send(`Usage: ${prefix}setchannel <#Channel>`);
    const channel = message.mentions.channels.first();
    config.DiscordData.channelId = channel.id;
    await fs.writeFile('config.json', JSON.stringify(config, ' ', 2))
           .catch(err => console.log(err))
           .then(message.reply(`Updated notification channel to: #${channel.name}`));
  }
});

async function checkStreams() {
  let streamingIndexes = [];
  for (let i = 0; i < Math.floor(streamers.length / 100) + 1; i++) {
    let userNames = [];
    for (let j = 0; j < Math.min(100, streamers.length); j++) {
      if (!streamers[i * 100 + j].isOut) userNames.push(streamers[i * 100 + j].twitchUserName);
    }
    try {
      for await (const stream of twitchClient.helix.streams.getStreamsPaginated({userName: userNames})) {
        const title = stream.title;
        if (title.toLowerCase().includes(config.TwitchData.keyphrase.toLowerCase())) {
          const userName = stream.userName;
          const streamerIndex = streamers.findIndex(s => s.twitchUserName.toLowerCase() === userName.toLowerCase());
          streamingIndexes.push(streamerIndex);
          if (streamerIndex === -1) {
            console.log("Couldn't find matching user?");
            continue;
          }
          if (!streamers[streamerIndex].isStreaming) {
            streamers[streamerIndex].isStreaming = true;
            if (config.DiscordData.channelId) {
              const user = await stream.getUser();
              const announcementEmbed = new Discord.MessageEmbed()
               .setAuthor(`${userName} is streaming right now!`, user.profilePictureUrl)
               .setTitle(`${title}`)
               .setURL(`https://twitch.tv/${userName}`)
               .addFields({ name: 'Game', value: stream.gameName })
               .setTimestamp()
               .setColor('#e91619')
               .setThumbnail(user.profilePictureUrl)
               .setImage(`https://static-cdn.jtvnw.net/previews-ttv/live_user_${userName}-1280x720.jpgs`);
              discordClient.channels.cache.get(config.DiscordData.channelId)
               .send(announcementEmbed);
            }
            let member = discordClient.channels.cache.get(config.DiscordData.channelId)
                          .guild.members.cache.get(streamers[streamerIndex].discordUserId);
            try {
              if (!member) member = await discordClient.channels.cache.get(config.DiscordData.channelId)
                                           .guild.members.fetch(streamers[streamerIndex].discordUserId);
            } catch (err) {
              streamers[i].isOut = true;
              console.log(`Looks like ${streamers[i].twitchUserName}(${streamers[i].discordUserId}) has left.`);
              // console.log(err);
            }
            if (config.DiscordData.roleId && member) {
              if (!member.roles.cache.some(r => r.id === config.DiscordData.roleId))
              member.roles.add(config.DiscordData.roleId);
            }
            await fs.writeFile('streamers.json', JSON.stringify(streamers, ' ', 2))
                   .catch(err => console.log(err));
          }
        }
      }
    } catch (err) {
      // console.log(err); // something with twitch node library 
    }
  }

  for (let i = 0; i < streamers.length; i++) {
    if (!streamingIndexes.includes(i) && !streamers[i].isOut) {
      streamers[i].isStreaming = false;
      let member = discordClient.channels.cache.get(config.DiscordData.channelId)
                    .guild.members.cache.get(streamers[i].discordUserId);
      try {
        if (!member) member = await discordClient.channels.cache.get(config.DiscordData.channelId)
                                     .guild.members.fetch(streamers[i].discordUserId);
      } catch (err) {
        streamers[i].isOut = true;
        console.log(`Looks like ${streamers[i].twitchUserName}(${streamers[i].discordUserId}) has left.`);
        // console.log(err);
      }
      if (config.DiscordData.roleId && member) {
        if (member.roles.cache.some(r => r.id === config.DiscordData.roleId)) {
          member.roles.remove(config.DiscordData.roleId);
        }
      }
      await fs.writeFile('streamers.json', JSON.stringify(streamers, ' ', 2))
             .catch(err => console.log(err));
    }
  }
}

discordClient.login(config.DiscordData.token);